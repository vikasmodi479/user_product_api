﻿using Microsoft.EntityFrameworkCore;

using UserAPI.Model;


namespace UserAPI.Context
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> context) : base(context)
        {

        }
        public DbSet<User> users { get; set; }
    }
}
